import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-proba02',
  templateUrl: 'appProba02.component.html',
  styles: [`
    h3 {
     color: red;
    }
  `]
})

export class AppProba02Component implements OnInit {

  constructor() {}

  ngOnInit() {}
}
