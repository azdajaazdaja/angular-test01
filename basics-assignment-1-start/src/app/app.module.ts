import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { AppProba01Component } from './appProba01/appProba01.component';
import { AppProba02Component } from './appProba02/appProba02.component';

@NgModule({
  declarations: [
    AppComponent,
    AppProba01Component,
    AppProba02Component
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
